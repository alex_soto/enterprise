# Hoverla Enterprise #

This workspace contains the server side Enterprise components 

## How to run ? ##

Samples are tested on Wildfly and GlassFish using the Arquillian ecosystem.

Only one container profile and one profile for browser can be active at a given time otherwise there will be dependency conflicts.

There are 4 available container profiles:

* ``wildfly-managed-arquillian``
    
    The default profile and it will install a Wildfly server and start up the server per sample.
    Useful for CI servers.

* ``wildfly-remote-arquillian``
    
    This profile requires you to start up a Wildfly server outside of the build. Each sample will then
    reuse this instance to run the tests.
    Useful for development to avoid the server start up cost per sample.

* ``glassfish-embedded-arquillian``
    
    This profile uses the GlassFish embedded server and runs in the same JVM as the TestClass.
    Useful for development, but has the downside of server startup per sample.

* ``glassfish-remote-arquillian``
    
    This profile requires you to start up a GlassFish server outside of the build. Each sample will then
    reuse this instance to run the tests.
    Useful for development to avoid the server start up cost per sample.

Each of the containers allow you to override the version used

* `-Dorg.wildfly=8.1.0.Final`

    This will change the version from 8.0.0 to 8.1.0.Final for WildFly.

* `-Dglassfish.version=4.1`

    This will change the version from 4.0 to 4.1 for GlassFish testing purposes.

Similarly, there are 6 profiles to choose a browser to test on:

* ``browser-firefox``
    
    To run tests on Mozilla Firefox. If its binary is installed in the usual place, no additional information is         required.

* ``browser-chrome``
    
    To run tests on Google Chrome. Need to pass a ``-Darq.extension.webdriver.chromeDriverBinary`` property
    pointing to a ``chromedriver`` binary.

* ``browser-ie``
    
    To run tests on Internet Explorer. Need to pass a ``-Darq.extension.webdriver.ieDriverBinary`` property
    pointing to a ``IEDriverServer.exe``.

* ``browser-safari``
    
    To run tests on Safari. If its binary is installed in the usual place, no additional information is required.

* ``browser-opera``
    
    To run tests on Opera. Need to pass a ``-Darq.extension.webdriver.opera.binary`` property pointing to a Opera        executable.

* ``browser-phantomjs``
    
    To run tests on headless browser PhantomJS. If you do not specify the path of phantomjs binary via 
    ``-Dphantomjs.binary.path`` property, it will be downloaded automatically.

To run them in the console do:

1. In the terminal, ``mvn -Pwildfly-managed-arquillian,browser-firefox test`` at the top-level directory to start the tests

When developing and runing them from IDE, remember to activate the profile before running the test.

To learn more about Arquillian please refer to the [Arquillian Guides](http://arquillian.org/guides/)

### Importing in Eclipse ###

To import the samples in an Eclipse workspace, please install the [Groovy plugins for your Eclipse version](http://groovy.codehaus.org/Eclipse+Plugin) first, then import the sample projects you want using File>Import>Existing Maven Projects. 


### Some coding principles ###


