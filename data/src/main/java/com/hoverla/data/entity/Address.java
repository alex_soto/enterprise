package com.hoverla.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@SuppressWarnings({ "serial" })
@Entity
@Table(name="ADDRESS")
public class Address extends BaseEntity {
	
	@Column(name="line1", length=100, nullable = false)
	private String line1;
	@Column(length = 100, nullable = false)
	private String line2;
	@Column(length = 100)
	private String line3; 
	@Column(length = 100, nullable = false)
	private String city;
	@Column(length = 100, nullable = false)
	private String state;
	@Column(name="postal_code", length = 20, nullable = false)
	private String postalCode;
	
	public Address() {
	}

	public String getAddressLine1() {
		return line1;
	}

	public void setAddressLine1(String addressLine1) {
		this.line1 = addressLine1;
	}

	public String getAddressLine2() {
		return line2;
	}

	public void setAddressLine2(String addressLine2) {
		this.line2 = addressLine2;
	}

	public String getAddressLine3() {
		return line3;
	}

	public void setAddressLine3(String addressLine3) {
		this.line3 = addressLine3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return postalCode;
	}

	public void setZip(String zip) {
		this.postalCode = zip;
	}
   
}
