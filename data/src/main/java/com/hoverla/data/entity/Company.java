package com.hoverla.data.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *  A Company that owns stores.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "COMPANY")
public class Company extends BaseEntity {

    @Column(length=128)
    @NotNull
    @Size(min = 1)
    private String name;
    
    @ManyToOne(targetEntity=Address.class, cascade=CascadeType.ALL)
	@JoinColumn(name="billing_address_id")
	private Address billingAddress;

    @ManyToOne(targetEntity=Address.class, cascade=CascadeType.ALL)
	@JoinColumn(name="mailing_address_id")
	private Address mailingAddress;
    
    @OneToMany(cascade=CascadeType.ALL)
    private List<Contact> contacts;
    
	@OneToMany(cascade = CascadeType.ALL)
	private List<Store> stores = new ArrayList<Store>();

    public Company() { }
    
    public Company(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Address getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(Address mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public List<Store> getStores() {
		return stores;
	}

	public void setStores(List<Store> stores) {
		this.stores = stores;
	}

	@Override
    public boolean equals(Object obj) {
        if (null == obj)
            return false;
        if (!(obj instanceof Company))
            return false;
        Company that = (Company)obj;
        if (that.name.equals(this.name) && that.getId() == this.getId())
            return true;
        else
            return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId(), this.name);
    }
    
    
}
