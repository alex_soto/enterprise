package com.hoverla.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *	A Contact, a person representing a store or a company
 */
@SuppressWarnings("serial")
@Entity
@Table(name="CONTACT")
public class Contact extends BaseEntity {

	@Column(length = 12)
	private String personGender; 

	@Column(length=32)
    @NotNull
    @Size(min = 1)
	private String personGivenName;

	@Column(length=4)
    @NotNull
    @Size(min = 1)
	private String personMiddleName;

	@Column(length=4)
    @NotNull
    @Size(min = 1)
	private String personSurName;
	
	@Column(length=4)
    @NotNull
    @Size(min = 1)
	private String personNamePrefixText;
	
	@Column(length=4)
    @NotNull
    @Size(min = 1)
	private String personNameSuffixText;

	@Column(length=64)
	private String title;
	
	public Contact() {
	}

	public String getPersonGender() {
		return personGender;
	}

	public void setPersonGender(String personGender) {
		this.personGender = personGender;
	}

	public String getPersonGivenName() {
		return personGivenName;
	}

	public void setPersonGivenName(String personGivenName) {
		this.personGivenName = personGivenName;
	}

	public String getPersonMiddleName() {
		return personMiddleName;
	}

	public void setPersonMiddleName(String personMiddleName) {
		this.personMiddleName = personMiddleName;
	}

	public String getPersonSurName() {
		return personSurName;
	}

	public void setPersonSurName(String personSurName) {
		this.personSurName = personSurName;
	}

	public String getPersonNamePrefixText() {
		return personNamePrefixText;
	}

	public void setPersonNamePrefixText(String personNamePrefixText) {
		this.personNamePrefixText = personNamePrefixText;
	}

	public String getPersonNameSuffixText() {
		return personNameSuffixText;
	}

	public void setPersonNameSuffixText(String personNameSuffixText) {
		this.personNameSuffixText = personNameSuffixText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
