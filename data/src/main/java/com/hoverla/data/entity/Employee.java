package com.hoverla.data.entity;

import com.hoverla.data.entity.BaseEntity;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity implementation class for Entity: Employee
 */
@SuppressWarnings("serial")
@Entity
public class Employee extends BaseEntity implements Serializable {

    @Column(length=64)
    @NotNull
    @Size(min = 1)
    private String name;

	@ManyToOne(targetEntity=Store.class, cascade = CascadeType.ALL)
	@JoinColumn(name="StoreId",
			referencedColumnName="id",
			insertable = true, 
			updatable = true, 
			nullable = true)
	private Store store;
	
	@ManyToOne(targetEntity=Company.class, cascade = CascadeType.ALL)
	@JoinColumn(name="CompanyId",
			referencedColumnName="id",
			insertable = true, 
			updatable = true, 
			nullable = false)
	private Company company;

	
	public Employee() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
   
}
