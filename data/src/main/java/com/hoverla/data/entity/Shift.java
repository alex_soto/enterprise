package com.hoverla.data.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity implementation class for Entity: Shift
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "SHIFT")
@NamedQueries({@NamedQuery(
	name = "Shift.findAll",  
	query = "SELECT s FROM Shift s WHERE s.store = :store Order By s.startTime"
		)
})
public class Shift extends BaseEntity {

    @Column(length=40)
    @NotNull
    @Size(min = 1)
    private String name;

	@Temporal(TemporalType.TIME)
	private Date startTime;

	@ManyToOne(targetEntity=Store.class, cascade = CascadeType.ALL)
	@JoinColumn(name="StoreId",
			referencedColumnName="id",
			insertable = true, 
			updatable = true, 
			nullable = false)
	private Store store;

	public Shift() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

}
