package com.hoverla.data.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity implementation class for Entity: Store
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "COMPANY")
public class Store extends BaseEntity {

    @Column(length=128)
    @NotNull
    @Size(min = 1)
    private String name;

    @ManyToOne(targetEntity=Company.class, cascade=CascadeType.ALL)
	private Company company;

    @ManyToOne(targetEntity=Address.class, cascade=CascadeType.ALL)
	@JoinColumn(name="billing_address_id")
	private Address billingAddress;

    @ManyToOne(targetEntity=Address.class, cascade=CascadeType.ALL)
	@JoinColumn(name="mailing_address_id")
	private Address mailingAddress;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Shift> shifts = new ArrayList<Shift>();
	
	public Store() {}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Shift> getShifts() {
		return shifts;
	}

	public void setShifts(List<Shift> shifts) {
		this.shifts = shifts;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Address getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(Address mailingAddress) {
		this.mailingAddress = mailingAddress;
	}
   
}
