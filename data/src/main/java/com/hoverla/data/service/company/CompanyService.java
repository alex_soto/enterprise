package com.hoverla.data.service.company;

import java.util.List;

import javax.ejb.Local;

import com.hoverla.data.entity.Company;

@Local
public interface CompanyService {
	Company create(String name);
	int add(Company acc);
    List<Company> getAll();
    void delte(int companyId);
    void activate(int companyId);
    void deactivate(int companyId);
    Company getCompany(int companyId);
    Company getByDeviceId(int deviceId);
}
