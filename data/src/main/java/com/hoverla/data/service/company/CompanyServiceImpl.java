/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hoverla.data.service.company;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.hoverla.data.entity.Company;

/**
 *  Company Data Service 
 */
@Stateless
public class CompanyServiceImpl implements CompanyService {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Company> getAll() {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Company> query = builder.createQuery(Company.class);
		Root<Company> c = query.from(Company.class);
		query.select(c).orderBy(builder.asc(c.get("name")));
		return em.createQuery(query).getResultList();
    }
    
	@Override
    public Company create(String name) {
        return new Company(name);
    }

	@Override
	public int add(Company acc) {
		em.persist(acc);
		return acc.getId();
	}

	@Override
	public void delte(int companyId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void activate(int companyId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deactivate(int companyId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Company getCompany(int companyId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Company getByDeviceId(int deviceId) {
		// TODO Auto-generated method stub
		return null;
	}

}
