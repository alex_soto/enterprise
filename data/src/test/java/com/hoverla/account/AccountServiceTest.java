package com.hoverla.account;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ShouldMatchDataSet;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.hoverla.data.entity.Company;
import com.hoverla.data.maintenance.LiquibaseStarter;
import com.hoverla.data.service.company.CompanyService;

@RunWith(Arquillian.class)
public class AccountServiceTest {

	/**
	 * Arquillian specific method for creating a file which can be deployed
	 * while executing the test.
	 *
	 * @return a war file
	 */
	@Deployment
	public static Archive<?> deployment() {
		JavaArchive result = ShrinkWrap.create(JavaArchive.class)
				.addClass(LiquibaseStarter.class)
				.addPackages(true, "com.hoverla.data")
//				.addPackages(true, "com.hoverla.data.service")
				.addAsResource("liquibase")
				.addAsManifestResource("META-INF/test-persistence.xml", "persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	
		System.out.println(result.toString(true));
		return result;
	}
	
	@EJB
	private CompanyService svc;

	void createDatabase() {
//		DBCDataSource dataSource = new JDBCDataSource();
//		dataSource.setDatabase("jdbc:hsqldb:file:/tmp/hsqldb/testdb");
//		dataSource.setUser("SA");
//		Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(dataSource.getConnection()));
//		Liquibase liquibase = new Liquibase("user-db.xml", new ClassLoaderResourceAccessor(), database);
//		liquibase.update(null);
	}
	
	/**
	 * Test of withdraw method, of class AccountSessionBean.
	 */
	@Test
	@ShouldMatchDataSet(value = "company/pepe.yml", excludeColumns = "id")
	public void createNewAccount() {
		final Company actual = new Company();
		actual.setName("Pepe");
		int id = svc.add(actual);
		System.out.println("New account id =" + id);
	}
	
	@Test
	@UsingDataSet("company/companies.yml")
	public void findAll() {
		List<Company> list = svc.getAll();
		assertEquals(3, list.size());
		assertEquals("Albert", list.get(0).getName());
	}
}