package com.hoverla.company;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.hoverla.data.entity.Company;
import com.hoverla.data.service.company.CompanyService;

@Path("company")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_XML)
@Stateless
public class CompanyServiceRest {
    @EJB
    private CompanyService svc;
    @Context
    private UriInfo uriInfo;

    @GET
    public Response getAll() {
    	List<Company> list = svc.getAll();
    	List<CompanyDTO> result = new ArrayList<CompanyDTO>(list.size());
    	for (Company c : list) {
    		result.add(translate(c));
    	}
    	GenericEntity<List<CompanyDTO>> entity = new GenericEntity<List<CompanyDTO>>(result) {};
    	return Response.ok().entity(entity).build();
    }
    
    private CompanyDTO translate(Company c) {
    	CompanyDTO result = new CompanyDTO();
    	result.setId(c.getId());
    	result.setName(c.getName());
		return result;
	}

	@POST
    public Response create(Company acc) {
    	int id = svc.add(acc);
    	URI bookUri = uriInfo.getAbsolutePathBuilder().path(Integer.toString(id)).build();
    	return Response.created(bookUri).build();
    }

}
