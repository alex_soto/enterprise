package com.hoverla.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.persistence.Cleanup;
import org.jboss.arquillian.persistence.DataSource;
import org.jboss.arquillian.persistence.TestExecutionPhase;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.hoverla.MyApplication;
import com.hoverla.company.CompanyServiceRest;
import com.hoverla.data.entity.Company;
import com.hoverla.data.service.company.CompanyService;
import com.hoverla.data.service.company.CompanyServiceImpl;

@RunWith(Arquillian.class)
@DataSource("java:jboss/datasources/ExampleDS")
@Cleanup(phase = TestExecutionPhase.NONE)  
@UsingDataSet("company/companies.yml")
@Ignore
public class AccountRestfulTest {

	@Deployment(testable = true)
	public static WebArchive deployment() {
		return ShrinkWrap.create(WebArchive.class)
				.addAsLibraries(
						Maven.resolver()
						.loadPomFromFile("pom.xml")
						.resolve("com.hoverla.enterprise:data")
						.withTransitivity()
						.asSingleFile()
				)
//				.addClass(Company.class)
//				.addClass(CompanyService.class)
//				.addClass(CompanyServiceImpl.class)
				.addClass(CompanyServiceRest.class)
				.addClass(MyApplication.class)
//				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	
    @Test
    @InSequence(1)
    public void setupDB_ARQ1077_Workaround()
    {
    }
	
    private WebTarget setUp(URL base) throws MalformedURLException {
		Client client = ClientBuilder.newClient();
		System.out.println(base);
		URL api = new URL(base, "api/company");
		System.out.println(api);
		
		URI uri = URI.create(api.toExternalForm());
		System.out.println(uri);
		
		WebTarget target = client.target(uri);
		target.register(Company.class);
		return target;
    }
    
	@Test
	@RunAsClient
	@InSequence(2)
    public void testGet(@ArquillianResource URL base) throws MalformedURLException {
		WebTarget target = setUp(base);
		
		GenericType<List<Company>> accountListType = new GenericType<List<Company>>() {};

		Response response = target.request(MediaType.APPLICATION_XML).get();
		assertNotNull(response);
		assertTrue("response=" + response.getStatusInfo(), response.getStatusInfo() == Response.Status.OK);
		List<Company> list = response.readEntity(accountListType);
		
		assertNotNull(list);
		assertEquals(3, list.size());
        assertEquals("Albert", list.get(0).getName());
        assertEquals("Penny", list.get(1).getName());
        assertEquals("Sheldon", list.get(2).getName());
    }
	
	@Test
	@RunAsClient
	@InSequence(3)
	@Ignore
    public void testCreate(@ArquillianResource URL base) throws MalformedURLException {
		
		WebTarget target = setUp(base);
		
		Company acc = new Company();
		acc.setName("Pepe");
		
		Response response = target.request(MediaType.APPLICATION_XML).post(Entity.entity(acc, MediaType.APPLICATION_XML));
		assertTrue(response.getStatusInfo() == Response.Status.OK);
		System.out.println(response.getEntity());
	}
}
